var sql = require('../connection');

var Simptome = function(simptome){
    this.Temperatura= simptome.Temperatura;
    this.Kaselj=simptome.Kaselj;
    this.Kihanje=simptome.Kihanje;
    this.Bolecegrlo-simptome.Bolecegrlo;
    this.Glavobol=simptome.Glavobol;
    this.Izcedek=simptome.Izcedek;
    this.Bolecinaprsih=simptome.Bolecinaprsih;
    this.Trajanje=simptome.Trajanje;
    this.Srbec=simptome.Srbec;
};

Simptome.dodaj = function (simptome, result) {    
    sql.query("INSERT INTO Simptomi set ?", simptome, function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res.insertId);
                result(null, res.insertId);
            }
        });           
};


Simptome.get = function (id_Simptomi, result) {
    sql.query('select Naziv, idDijagnoza from sd, mydb.simptomi s where sd.Temperatura=s.Temperatura and sd.Kaselj=s.Kaselj and sd.Glavobol=s.Glavobol and sd.Kihanje=s.Kihanje and s.id_Simptomi=? and sd.`Bolecina v prsih`=s.`Bolecina v prsih` and sd.`Izcedek  iz  nosu`=s.`Izcedek  iz  nosu` and sd.`Srbeč nos/oči`=s.`Srbeč nos/oči` and sd.`Bolece grlo`=s.`Bolece grlo` ', [id_Simptomi], function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else if (res.length > 0  ) {
            result(null, res);
        } else {
           
            result(err, null);
        }		
})
};


Simptome.pridobi = function (id_Simptomi, result) {
    sql.query('select Opis, Zdravljenje  from sd, mydb.simptomi s where sd.Temperatura=s.Temperatura and sd.Kaselj=s.Kaselj and sd.Glavobol=s.Glavobol and sd.Kihanje=s.Kihanje and s.id_Simptomi=? and sd.`Bolecina v prsih`=s.`Bolecina v prsih` and sd.`Izcedek  iz  nosu`=s.`Izcedek  iz  nosu` and sd.`Srbeč nos/oči`=s.`Srbeč nos/oči` and sd.`Bolece grlo`=s.`Bolece grlo` ', [id_Simptomi], function (err, res) {             
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                result(null, res);
          
            }
        });   
};


Simptome.pridobi = function (id_Simptomi, result) {
    sql.query('select Opis, Zdravljenje  from sd, mydb.simptomi  where sd.Temperatura=simptomi.Temperatura and sd.Kaselj=simptomi.Kaselj and sd.Glavobol=simptomi.Glavobol and sd.Kihanje=simptomi.Kihanje and simptomi.id_Simptomi=? and sd.`Bolecina v prsih`=simptomi.`Bolecina v prsih` and sd.`Izcedek  iz  nosu`=simptomi.`Izcedek  iz  nosu` and sd.`Srbeč nos/oči`=simptomi.`Srbeč nos/oči` and sd.`Bolece grlo`=simptomi.`Bolece grlo` ', [id_Simptomi], function (err, res) {             
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                result(null, res);
          
            }
        });   
};

module.exports=Simptome;
var sql = require('../connection');
var async = require('async');
var podaciChart = function (podaciChart) {
    this.Mesec = podaciChart.Mesec;
    this.Leto = podaciChart.Leto;
};
podaciChart.Leto = function (Leto, result) {
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 1 AND YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 2 AND YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza AND dijagnoza.Naziv = \"Covid-19\" AND MONTH(Datum_Zacetka) = 3 AND YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 4 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 5 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 6 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 7 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 8 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 9 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 10 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 11 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    ),
    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 12 && YEAR(Datum_Zacetka) = ?", [Leto],
        function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                console.log(res);
            }
        }
    )

};

module.exports = podaciChart;
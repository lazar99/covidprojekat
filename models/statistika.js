var sql = require('../connection');

var Statistika = function(statistika){
    this.SteviloNovih = statistika.SteviloNovih;
    this.SteviloPotrjenih = statistika.SteviloPotrjenih;
    this.SteviloOzdravljenih = statistika.SteviloOzdravljenih;
    this.SteviloUmrlih = statistika.SteviloUmrlih;
    this.Datum = new Date();
};

Statistika.return = function (result) {
    sql.query("Select * from Statistika  WHERE idStatistika=1", function (err, res) {             
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                result(null, res);
            }
        });   
};
Statistika.izmeni=function (podaci,result) {
    sql.query("UPDATE statistika SET SteviloNovih = ? ,  SteviloPotrjenih = ? , SteviloOzdravljenih = ? , SteviloUmrlih = ? , Datum = ?  WHERE idStatistika = 1", [podaci.SteviloNovih, podaci.SteviloPotrjenih,podaci.SteviloOzdravljenih,podaci.SteviloUmrlih, new Date().toISOString().slice(0, 19).replace('T', ' ')], function (err, res)  {             
        if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                result(null, res);
          
            }
        });   
};
module.exports= Statistika;
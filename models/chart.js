//<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
var sql = require('../connection');
var chart = function(){
    this.Mesec = chart.Mesec;
    this.Leto = chart.Leto;
};

//let chartBolezni = document.getElementById('chartBolezni').getContext('2d');
let chartBolezni;
chart.izrisi = function (podaci, result) {
    let chartMesec = new Chart(chartBolezni, {
        type: bar,
        data: {
            labels: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
            datasets: [{
                label: ["Chart Bolezni"],
                data:[
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 1 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 2 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 3 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 4 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 5 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 6 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 7 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 8 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 9 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 10 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 11 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto],
                    sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && MONTH(Datum_Zacetka) = 12 && YEAR(Datum_Zacetka) = ?"), [podaci.Leto]
                ],
                backgroundColor: 'Maroon',
                borderWidth: 1,
                borderColor: '#C0C0C0',
                hoverBorderWidth: 3,
                hoverBorderColor: '#708090'
            }]
        },
        options: {
            title:{
                display:true,
                text:'Prikaz okuzenih po mesecu',
                fontSize: 23
            },
            legend:{
                display: false,
                /*
                position: 'right',
                labels:{
                    fontColor: 'black'
                }
                */
            },
            toolTips:{
                enabled:false
            }
        }
    
    }); 
};


let chartLeto = new Chart(chartBolezni, {
    type: bar,
    data: {
        labels: ['2018', '2019', '2020'],
        datasets: [{
            label: ["Chart Bolezni"],
            data:[
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = 2018"),
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = 2019"),
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Covid-19\" && YEAR(Datum_Zacetka) = 2020"),
            ],
            backgroundColor: 'Maroon',
            borderWidth: 1,
            borderColor: '#C0C0C0',
            hoverBorderWidth: 3,
            hoverBorderColor: '#708090'
        }]
    },
    options: {
        title:{
            display:true,
            text:'Prikaz okuzenih po letu',
            fontSize: 23
        },
        legend:{
            display: false,
            /*
            position: 'right',
            labels:{
                fontColor: 'black'
            }
            */
        },
        toolTips:{
            enabled:false
        }
    }

});
let chartSlovenija = new Chart(chartBolezni, {
    type: pie,
    data: {
        labels: ['Gripa', 'Alergija', 'Prehlada', 'Cancer'],
        datasets: [{
            label: 'Bolezni v sloveniji',
            data:[
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Gripa\" && MONTH(Datum_Zacetka) = ?"), [podaci.Mesec, podaci.Leto],
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Alergija\" && MONTH(Datum_Zacetka) = ?"), [podaci.Mesec, podaci.Leto],
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Prehlada\" && MONTH(Datum_Zacetka) = ?"), [podaci.Mesec, podaci.Leto],
                sql.query("SELECT COUNT(idBolezni) FROM bolezni, dijagnoza WHERE bolezni.Dijagnoza_idDijagnoza = dijagnoza.idDijagnoza && dijagnoza.Naziv = \"Cancer\" && MONTH(Datum_Zacetka) = ?"), [podaci.Mesec, podaci.Leto]
            ],
            backgroundColor: ['Maroon', 'Salmon', 'Olive', 'Violet'],
            borderWidth: 1,
            borderColor: '#C0C0C0',
            hoverBorderWidth: 3,
            hoverBorderColor: '#708090'
        }]
    },
    options: {
        title:{
            display:true,
            text:'Statistika v Sloveniji',
            fontSize: 23
        },
        legend:{
            display: true,
            position: 'right',
            labels:{
                fontColor: 'black'
            }
        },
        toolTips:{
            enabled:true
        }
    }

});

module.exports=chart;

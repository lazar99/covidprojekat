var sql = require('../connection');

var Bolezni = function(bolezni){
    this.PotrjenaDijagnoza=PotrjenaDijagnoza;
    this.DatumZacetka=DatumZacetka;
    this.DatumKonca=DatumKonca;
    this.Uporabnik_idUporabnik=Uporabnik_idUporabnik;
    this.Dijagnoza_idDijagnoza=Dijagnoza_idDijagnoza;
    this.Simptomi_id_Simptomi=Simptomi_id_Simptomi;
};

Bolezni.dodaj = function (bolezni, result) {    
    sql.query("INSERT INTO Bolezni set ?", bolezni, function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res.insertId);
                result(null, res.insertId);
            }
        });           
};

Bolezni.izbrisi = function(id, result){
    sql.query("DELETE FROM mydb.bolezni WHERE idBolezni = ?", [id], function (err, res) {

               if(err) {
                   console.log("error: ", err);
                   result(null, err);
               }
               else{
              
                result(null, res);
               }
           }); 
};

Bolezni.izmeni = function(id, result){
    sql.query("UPDATE mydb.bolezni b, sd, mydb.simptomi s SET Dijagnoza_idDijagnoza=idDijagnoza WHERE sd.Temperatura=s.Temperatura and sd.Kaselj=s.Kaselj and sd.Glavobol=s.Glavobol and sd.Kihanje=s.Kihanje and s.id_Simptomi=? and s.id_Simptomi=b.Simptomi_id_Simptomi and sd.`Bolecina v prsih`=s.`Bolecina v prsih` and sd.`Izcedek  iz  nosu`=s.`Izcedek  iz  nosu` and sd.`Srbeč nos/oči`=s.`Srbeč nos/oči` and sd.`Bolece grlo`=s.`Bolece grlo`", [id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };

  Bolezni.beri = function (Uporabnik_id_Uporabnik, result) {
    sql.query('select * from mydb.bolezni,mydb.dijagnoza where Uporabnik_idUporabnik=? and Dijagnoza_idDijagnoza=idDijagnoza ORDER BY Datum_Zacetka desc', [Uporabnik_id_Uporabnik], function (err, res) {             
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                result(null, res);
          
            }
        });   
};


Bolezni.potvrdi = function(id,result){
    sql.query("UPDATE bolezni SET PotrjenaDijagnoza = 1 WHERE idBolezni = ?", [id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };


module.exports=Bolezni;
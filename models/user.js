var sql = require('../connection');

var User = function(user){
    this.uporabnisko_ime = user.uporabnisko_ime;
    this.geslo = user.geslo;
    this.ime = user.ime;
    this.priimek = user.priimek;
    this.email = user.email;
    this.datum_rojstva = user.datum_rojstva;
    this.isAdmin = user.isAdmin;
};

User.dodaj = function (nov_uporabnik, result) {    
        sql.query("INSERT INTO Uporabnik set ?", nov_uporabnik, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};



User.check = function (uporabnisko_ime,email, result) {  
    sql.query("SELECT * FROM Uporabnik WHERE uporabnisko_ime = ? OR email = ?", [uporabnisko_ime,email ], function (err, res) {
        if (res.length <= 0 ) {
            console.log('User doesnt exist');
            result(null, res);

        } else {
            console.log('User exists');
            result(err, null);

        }			
        
    })

};



User.get = function (uporabnisko_ime,geslo, result) {  
    sql.query("SELECT * FROM Uporabnik WHERE uporabnisko_ime = ? && geslo = ?", [uporabnisko_ime, geslo], function (err, res) {
        if (res.length > 0 ) {
            console.log('Correct username and password');
            result(null, res);
        } else {
            console.log('Incorrect username and password');
            result(err, null);
        }			
        
    })

};





User.prijatelji = function (id, result) {  
    sql.query("SELECT * FROM uporabnik  LEFT JOIN prijatelj ON uporabnik.idUporabnik = prijatelj.Uporabnik_idUporabnik1 WHERE prijatelj.Uporabnik_idUporabnik = ? UNION SELECT * FROM uporabnik LEFT JOIN prijatelj ON uporabnik.idUporabnik = prijatelj.Uporabnik_idUporabnik WHERE prijatelj.Uporabnik_idUporabnik1 = ?;", [id,id], function (err, res) {
        if(res == null) {
            console.log("error: ", err);
            result(null);
        }
        else{
            console.log(res);
            result(null,res);
        }
    })    
};


User.get_prijatelj = function (id,email, result) {    
    sql.query("SELECT * FROM uporabnik WHERE uporabnik.email= ? ", [email], function (err, res, fields) {
                      
        if (res.length > 0 ) {
            console.log('Friend found');
            result(null, res);
        } else {
            console.log('Friend not found');
            result(err, null);
        }			
    })         
};

User.preveri = function (id,friend, result) {
    sql.query("SELECT * FROM prijatelj WHERE (prijatelj.Uporabnik_idUporabnik = ? AND prijatelj.Uporabnik_idUporabnik1 = ? ) OR (prijatelj.Uporabnik_idUporabnik = ? AND prijatelj.Uporabnik_idUporabnik1 = ? )",[id,friend,friend, id], function(err, res, fields) {
                        
        if (res.length > 0 ) {
            console.log('Already friends');
            result(err, null);
            
        } else {
            console.log('Friendship found');
            result(null, res);
        }			
    })         
};

User.prijatelj_dodaj = function (id,friend, result) {
    sql.query("INSERT INTO prijatelj (Uporabnik_idUporabnik, Uporabnik_idUporabnik1) VALUES ( ? , ? )",[id,friend], function(err, res, fields) {
                        
        if(err) {
            console.log("error: ", err);
        }
        else{
            console.log(res);
        }
    })           
};


User.izbrisi_prijatelja = function(id1,id2, result){
    sql.query("DELETE FROM prijatelj WHERE (Uporabnik_idUporabnik1 = ? AND Uporabnik_idUporabnik = ?) OR (Uporabnik_idUporabnik1 = ? AND Uporabnik_idUporabnik = ? )  ", [id1,id2,id2,id1], function (err, res) {

               if(err) {
                   console.log("error: ", err);
                   result(null, err);
               }
               else{
                result(null, res);
               }
           }); 
};

User.zbrisi_okruzbo = function(id1,id2,result){
    sql.query("UPDATE prijatelj SET okruzba1 = 0 WHERE Uporabnik_idUporabnik = ? AND Uporabnik_idUporabnik1 = ?",[id1,id2], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };

  User.zbrisi_okruzbo1 = function(id1,id2,result){
    sql.query("UPDATE prijatelj SET okruzba2 = 0 WHERE Uporabnik_idUporabnik = ? AND Uporabnik_idUporabnik1 = ?",[id1,id2], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };


  User.okruzbo = function(id, result){
    sql.query("UPDATE prijatelj SET okruzba1 = 1 WHERE  Uporabnik_idUporabnik = ?  ",[id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };

  User.okruzbo1 = function(id, result){
    sql.query("UPDATE prijatelj SET okruzba2 = 1 WHERE  Uporabnik_idUporabnik1 = ?  ",[id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                  result(null, err);
               }
             else{   
               result(null, res);
                  }
              }); 
  };


module.exports=User;